# LudumDare48

| master builds | stable builds            |
| ------------- |:------------------------:|
| ![](https://gitlab.com/fachschafttf/ludumdare48/badges/master/pipeline.svg)      | ![](https://gitlab.com/fachschafttf/ludumdare48/badges/stable/pipeline.svg) |

Project for the LudumDare 48. The theme is "Deeper and Deeper". More about the LudumDare on https://ldjam.com/

## Try it out

Play the latest nightly build on: https://fachschafttf.gitlab.io/ludumdare48/

## Thanks to open source developers

We have used a lot of free code. Special thanks goes out to these projects:

* Untity3d
* CI-Pipeline for Unity3d https://game.ci
* Blender
* firebase realtime database (highscore)
* Firebase Webgl in Unity: https://github.com/rotolonico/FirebaseWebGL
* Stackoverflow

## Project structure

our project follows this structure we decided on before starting any coding:

[Structure](structure.md)
