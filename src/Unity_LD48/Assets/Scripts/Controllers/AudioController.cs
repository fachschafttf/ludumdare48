using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController Instance;

    [Range(0, 10)]
    public int MusicVolume;
    [Range(0, 10)]
    public int SfxVolume;

    private Sound _currentMusicTrack;


    public Sound[] MusicTracks;
    public Sound[] SfxSounds;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            foreach (var sound in SfxSounds)
            {
                try
                {
                    sound.Stop();
                }
                catch
                {
                    continue;
                }
            }
            Destroy(gameObject);
            return;
        }

        LoadSounds();
        PlayMusic("MainMusic");
        PlaySound("Water");
    }


    private void LoadSounds()
    {
        foreach (var sound in SfxSounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.loop = sound.loop;
        }

        foreach (var sound in MusicTracks)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.loop = sound.loop;
        }
    }

    public void PlayMusic(string name)
    {
        var sound = Array.Find(MusicTracks, s => s.clip.name == name);
        Play(sound, (float)MusicVolume / 10, name);
        if (sound != null)
        {
            if (_currentMusicTrack != null)
            {
                _currentMusicTrack.source.Stop();
            }
            _currentMusicTrack = sound;
        }
    }

    public void SetSoundVolume(string name, float volume)
    {
        var sound = GetSound(name);
        sound.SetMasterVolume(SfxVolume * volume);
    }

    private Sound GetSound(string name)
    {
        return Array.Find(SfxSounds, s => s.clip.name == name);
    }


    public void PlaySound(string name, float volume = 1f)
    {
        var sound = GetSound(name);
        Play(sound, volume * (float)SfxVolume / 10, name);
    }

    private void Play(Sound sound, float volume, string name)
    {

        if (sound != null)
        {
            sound.Play(volume);
        }
        else
        {
            Debug.LogError("Clip " + name + " not found for playback");
        }
    }

    public void PlaySoundRelativeToCameraPosition(string soundName, Vector3 soundOriginPosition)
    {
        var camera = FindObjectOfType<CameraController>();
        if (!camera)
        {
            Debug.LogError("No Camera Controller found");
            return;
        }
        var distanceFromCamera = Vector3.Distance(camera.transform.position, soundOriginPosition);

        var farDistance = 4.8f;
        var closeDistance = 4.2f;


        var distanceSfxVolumeFactor = 1 - Mathf.Max(0, Mathf.Min((distanceFromCamera - closeDistance) / (farDistance - closeDistance), 1));

        // Sound Loudness with dezibel stuff
        var modifiedVolume = Mathf.Pow(Mathf.Clamp01(distanceSfxVolumeFactor), Mathf.Log(10, 4));

        PlaySound(soundName, modifiedVolume);
    }

    public void StopAllSfxSounds()
    {
        foreach (var sound in SfxSounds)
        {
            sound.Stop();
        }
    }

    public void ChangeMusicVolume(int change)
    {
        MusicVolume += change;
        EnsureMusicIntervals();
        _currentMusicTrack.SetMasterVolume((float)MusicVolume / 10);
    }

    public void ChangeSfxVolume(int change)
    {
        SfxVolume += change;
        EnsureMusicIntervals();
    }

    public void SetMusicVolume(int change)
    {
        MusicVolume = change;
        EnsureMusicIntervals();
        _currentMusicTrack.SetMasterVolume((float)MusicVolume / 10);
    }

    public void SetSfxVolume(int change)
    {
        SfxVolume = change;
        EnsureMusicIntervals();
        foreach (var sound in SfxSounds)
        {
            sound.SetMasterVolume((float)SfxVolume / 10);
        }
}

    private void EnsureMusicIntervals()
    {
        int minVolume = 0;
        int maxVolume = 10;

        MusicVolume = Mathf.Min(maxVolume, Mathf.Max(minVolume, MusicVolume));
        SfxVolume = Mathf.Min(maxVolume, Mathf.Max(minVolume, SfxVolume));
    }
}
