using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseObstacleController : MonoBehaviour
{
    public float hitForce = 400f;
    public float damageOnHit = 20f;

    private bool killInLateUpdate = false;
    public float speed = 500f;
    public float despawnAtZ = -30f;
    GameObject Target;

    private Rigidbody _rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        Target = GameObject.Find("Player");
        transform.LookAt(Target.transform);
        _rigidbody = GetComponent<Rigidbody>();
    }


    void FixedUpdate()
    {
        _rigidbody.AddForce(transform.forward * Time.deltaTime * speed, ForceMode.Acceleration);
    }

    private void LateUpdate()
    {
        if(killInLateUpdate)
        {
            Destroy(this.gameObject);
        }
        if (transform.position.z < despawnAtZ)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint contact = collision.GetContact(0);
        Collider other = contact.otherCollider;
        if (other.tag == "Player")
        {
            Vector3 dir = contact.point - transform.position;
            dir.y = dir.z = 0f;
            PlayerController player = other.gameObject.GetComponent<PlayerController>();
            player.addCollisionForce(dir.normalized * hitForce);
            player.damage(damageOnHit);
        }
        if (other.tag == "Wall")
        {
            killInLateUpdate = true;
        }
    }
}
