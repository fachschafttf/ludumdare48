using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingController : MonoBehaviour
{
    private Waves WaterSurface;
    private Collider leftBorder;
    private Collider rightBorder;
    private Rigidbody _rigidbody;

    public bool Flying;
    public float FallingAcceleration = 0.1f;
    public GameObject gameFieldBorders;
    public float spinAngle = 90f;
    public float centerForce = 0.5f;

    public float FloatingOffset;
    // Start is called before the first frame update
    void Start()
    {
        WaterSurface = GameObject.FindObjectOfType<Waves>();
        if (!gameFieldBorders)
        {
            gameFieldBorders = GameObject.Find("FieldBorders");
        }
        foreach (BoxCollider c in gameFieldBorders.GetComponents<BoxCollider>())
        {
            if (c.center.x < 0)
            {
                leftBorder = c;
            }
            if (c.center.x > 0)
            {
                rightBorder = c;
            }
        }
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Flying)
        {
            GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity - new Vector3(0, FallingAcceleration, 0);
            CheckLanding();
            return;
        }
        if (!WaterSurface)
        {
            Debug.LogError("WaterSurface or GameField not found");
            return;
        }

        //Debug.Log("Adjusting Height");
        AdjustHeight();
        AdjustAngle();
        
    }

    private void FixedUpdate()
    {
        AdjustCenterForce();
    }

    private void CheckLanding()
    {
        var position = transform.position;
        var waterHeight = WaterSurface.GetHeightAtPoint(position);
        if (position.y < waterHeight)
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
            Flying = false;
        }
    }

    private void AdjustAngle()
    {
        var scale = WaterSurface.transform.localScale.z;
        var position = transform.position;
        var positionYCenter = position.y;
        var positionYFront = WaterSurface.GetHeightAtPoint(position + new Vector3(0, 0, scale));

        var distLeft = (leftBorder.ClosestPoint(position) - position).magnitude;
        var distRight = (rightBorder.ClosestPoint(position) - position).magnitude;
        var relativeXPosition = distLeft / (distLeft + distRight);
        //Debug.LogFormat("left: {0}, right: {1}", distLeft, distRight);

        var angles = transform.eulerAngles;
        angles.x = -Mathf.Rad2Deg * Mathf.Atan((positionYFront - positionYCenter) / scale);
        angles.z = Mathf.Lerp(-spinAngle, spinAngle, relativeXPosition);
        transform.eulerAngles = angles;
    }

    private void AdjustHeight()
    {
        var position = transform.position;
        position.y = WaterSurface.GetHeightAtPoint(position) + FloatingOffset;

        transform.position = position;
    }

    private void AdjustCenterForce()
    {
        var position = transform.position;
        _rigidbody.AddForce((leftBorder.ClosestPoint(position) +
            rightBorder.ClosestPoint(position) - 2 * position) * centerForce, ForceMode.Acceleration);
    }

}
