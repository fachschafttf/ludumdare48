using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{ 
    public float rotationSmooth = 15f;
    public float difficulty = 1;

    public GameObject pipeGeneratorObj;
    public GameObject gameFieldBorders;
    public GameObject lampPrefab;
    public ObstacleSpawnController obstacleSpawn;
    public SewerGrateController[] sewerGrateControllers;

    private PipeGenerator pipeGenerator;

    private float elapsedLightTime;
    private float elapsedGameTime;
    private float lastDifficultyUpdate;
    private bool initLamps = true;
    // Start is called before the first frame update
    void Start()
    {
        if(pipeGeneratorObj != null)
            pipeGenerator = pipeGeneratorObj.GetComponent<PipeGenerator>();
        if (pipeGenerator == null || pipeGeneratorObj == null || gameFieldBorders == null)
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        elapsedGameTime += Time.deltaTime;
        elapsedLightTime += Time.deltaTime;
        lastDifficultyUpdate += Time.deltaTime;
        pipeGeneratorObj.transform.rotation = Quaternion.RotateTowards(pipeGeneratorObj.transform.rotation, Quaternion.Inverse(pipeGenerator.GetCurrentRotation()), rotationSmooth * Time.deltaTime);
        gameFieldBorders.transform.rotation = pipeGeneratorObj.transform.rotation;
        if (elapsedLightTime > 1f && lampPrefab != null)
        {
            PalceLamp(35);
            elapsedLightTime = 0;
        }

        if (initLamps)
        {
            initLamps = false;
            for(int i = 0; i < 6; i++)
            {
                PalceLamp(i * 7);
            }
        }

        if(lastDifficultyUpdate > 5)
        {
            lastDifficultyUpdate = 0;

            if(obstacleSpawn) {
                obstacleSpawn.spawnTime = 6 - (elapsedGameTime / 20) * difficulty;
            }

            foreach(SewerGrateController sewerGrate in sewerGrateControllers)
            {
                if (!sewerGrate)
                {
                    continue;
                }
                sewerGrate.respawnCoolDown = 5 - (elapsedGameTime / 20) * difficulty;
                sewerGrate.respawnRangeMax = (int)(80 - (elapsedGameTime / 10) * difficulty);
                sewerGrate.respawnRangeMin = (int)(40 - (elapsedGameTime / 20) * difficulty);
            }
        }

    }

    private void PalceLamp(int distance)
    {
        GameObject lamp = Instantiate(lampPrefab);
        lamp.transform.position = Vector3.zero;
        //cube.transform.parent = pipeGeneratorObj.transform;
        lamp.transform.localScale = Vector3.one * 4f;
        pipeGenerator.PlaceObjectInPipe(lamp, distance, new Vector2(0, 30));
        Destroy(lamp, 8);
    }
}
