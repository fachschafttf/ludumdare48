using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeGenerator : MonoBehaviour
{
    public int segmentCount = 100;
    public float stepLength = 3f;
    public float speed = 3f;
    public float smoothTime = 0.3F;
    public float rotationSmooth = 5;
    public float pipeWidth = 10f;
    public bool renderPath = false;

    public GameObject waterPlanePrefab;

    public Material lineMaterial;
    private PipePath pathGenerator;
    private List<Vector3> path;
    private Vector3[] targetPath;
    private Vector3[] startPath;
    private int currentSegment = 0;
    private PipeMesh pipeMesh;
    private Watermesh waterMesh;
    private MeshFilter meshFilter;
    private MeshFilter waterMeshFilter;

    private LineRenderer pathLineRender;
    private Quaternion targetRotation;
    Quaternion currentRotation;
    private Vector3 rotationStep;
    private int rotationStepCounter = 0;
    private Vector3[] velocities;
    private Material pipeMaterial;

    private List<ObservingObject> observedGameobjects;
    // Start is called before the first frame update
    void Start()
    {
        observedGameobjects = new List<ObservingObject>();
        pipeMaterial = GetComponent<MeshRenderer>().materials[0];
        Random.InitState((int)System.DateTime.Now.Ticks);
        pathGenerator = new PipePath();
        path = new List<Vector3>(pathGenerator.generatePath(segmentCount, stepLength));
        /*
        path = new List<Vector3>() {
            new Vector3(0, 0, 0) + transform.position,
            new Vector3(0, 0, 20) + transform.position,
            new Vector3(0, 0, 30) + transform.position,
            new Vector3(2, 0, 40) + transform.position,
            new Vector3(0, 0, 50) + transform.position,
            new Vector3(-5, 0, 60) + transform.position,
            new Vector3(5, 0, 70) + transform.position
        };
        */
        GameObject pathLine = new GameObject();
        pathLine.transform.SetParent(transform);
        pathLine.AddComponent<LineRenderer>();
        pathLineRender = pathLine.GetComponent<LineRenderer>();
        pathLineRender.material = lineMaterial;
        pathLineRender.SetColors(Color.red, Color.red);
        pathLineRender.SetWidth(0.1f, 0.1f);
        pathLine.SetActive(renderPath);

        GameObject waterPlane = Instantiate(waterPlanePrefab, transform);
        waterMeshFilter = waterPlane.GetComponent<MeshFilter>();
        
        pipeMesh = new PipeMesh();
        Mesh newMesh = pipeMesh.GeneratePipeOfPath(path.ToArray(), pipeWidth, 16, transform.position);
        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = newMesh;
        targetRotation = Quaternion.identity;
        targetPath = path.ToArray();
        velocities = new Vector3[path.Count];
        for(int i = 0; i < path.Count; i++)
        {
            velocities[i] = Vector3.zero;
        }
        startPath = path.ToArray();
        waterMesh = new Watermesh();
        waterMeshFilter.mesh = waterMesh.GeneratePipeOfPath(path.ToArray(), pipeWidth, 16, transform.position);
        
        RenderPathLine();
    }

    // Update is called once per frame
    void Update()
    {
        PipeMovement();
        RenderPathLine();
    }


    private void PipeMovement()
    {
        Vector3 step = path[1].normalized * speed * Time.deltaTime;
        Vector3 targetStep = Vector3.forward * speed * Time.deltaTime;
        //Vector3[] vertices = GetComponent<MeshFilter>().mesh.vertices;
        for (int i = 1; i < path.Count - 1; i++)
        {
            path[i] -= step;
            targetPath[i] -= targetStep;
            startPath[i] -= targetStep;
           
            /*
            for (int j = 0; j <= 16; j++)
            {
                vertices[i * 17 + j] -= step;
            }
            */
        
        }

       
        if (path[1].sqrMagnitude <= 1f || path[1].z <= -1f)
        {
            path.RemoveAt(0);
            pipeMesh.removedSegments++;
            if (path.Count % pathGenerator.connectedParts == 0)
            {
                path.RemoveAt(path.Count - 1);
                Vector3[] newPart = pathGenerator.AppendPath(path.ToArray(), stepLength);
                for(int i = 0; i < newPart.Length; i++)
                {
                    path.Add(newPart[i]);
                }
            }

            targetPath = path.ToArray();
            startPath = path.ToArray();
            velocities = new Vector3[path.Count];
            foreach (ObservingObject observedGameObject in observedGameobjects.ToArray())
            {
                observedGameObject.pathIndex--;
                if (observedGameObject.pathIndex <= 0)
                {
                    observedGameObject.gameObject.SendMessage("LeftPipe", SendMessageOptions.DontRequireReceiver);
                    observedGameobjects.Remove(observedGameObject);
                }
            }

            // Quaternion rotation = Quaternion.FromToRotation(path[currentSegment+1], path[currentSegment]);
            Vector3 relativePos = path[1] - path[0];
            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            // Debug.Log(rotation.eulerAngles);
            //transform.rotation = rotation;
            //path[currentSegment+1] = rotation * path[currentSegment+1];
            /*
            for(int i = path.Length - 1; i > currentSegment + 1; i--)
            {
                Vector3 shift = path[i] - path[i - 1];
                Vector3 translated = path[i] - shift;
                translated = Quaternion.Inverse(rotation) * translated;
                path[i] = translated + shift;
            }
            */
            targetRotation = Quaternion.Inverse(rotation);
            //rotationStep = targetRotation / rotationSmooth;
            //rotationStepCounter = rotationSmooth;
            // transform.rotation = vecRot;
            
            for (int i = 0; i < path.Count; i++)
            {
                //Quaternion currentRot = Quaternion.LookRotation(path[i], Vector3.up);
                //Quaternion vecRot = Quaternion.RotateTowards(currentRot, targetRotation, 1f * Time.deltaTime); ;
                targetPath[i] = (targetRotation) * path[i];
                velocities[i] = Vector3.zero;
            }
        }
        /*
        if (rotationStepCounter > 0)
        {
            for (int i = 0; i < path.Count; i++) { 
                targetPath[i] = Quaternion.Euler(rotationStep) * targetPath[i];
            }
            rotationStepCounter--;
        }
        */
        Vector3 relativeCurrentPos = path[4] - path[0];
        Quaternion smoothTargetRot = Quaternion.Inverse(Quaternion.LookRotation(relativeCurrentPos, Vector3.up));
        relativeCurrentPos = path[2] - path[0];
        currentRotation = Quaternion.Inverse(Quaternion.LookRotation(relativeCurrentPos, Vector3.up));
        currentRotation = Quaternion.RotateTowards(Quaternion.LookRotation(Vector3.forward, Vector3.up), currentRotation, rotationSmooth * Time.deltaTime);
        for (int i = 0; i < path.Count-1; i++)
        {
            //path[i] = targetPath[i];

            // path[i] = Vector3.SmoothDamp(path[i], targetPath[i], ref velocities[i], smoothTime);
          
            path[i] = currentRotation * path[i];
            //path[i] = targetPath[i];
        }

        foreach (ObservingObject observedGameObject in observedGameobjects)
        {
            observedGameObject.updatePos(path, gameObject.transform);
      
        }

        RenderPathLine();
        //mesh.vertices = vertices;
        //mesh.RecalculateBounds();
        meshFilter.mesh = pipeMesh.GeneratePipeOfPath(path.ToArray(), pipeWidth, 16, transform.position);
        //meshFilter.mesh.uv = pipeMesh.GenerateUVs((path[1].z - path[0].z) / (stepLength*5));
        //UpdateUVs();
        meshFilter.mesh.RecalculateBounds();
        waterMeshFilter.mesh = waterMesh.GeneratePipeOfPath(path.GetRange(11, path.Count - 12).ToArray(), pipeWidth, 16, transform.position);
        waterMeshFilter.mesh.RecalculateBounds();
        
    }

    void LateUpdate()
    {
        //UpdateUVs();
    }

    private void UpdateUVs()
    {
        pipeMaterial.SetTextureOffset("_MainTex", new Vector2(path[1].z, 0));
    }


    private void RenderPathLine()
    {
        if (renderPath)
        {
            pathLineRender.positionCount = path.Count;
            for (int i = 0; i < path.Count; i++)
            {
                pathLineRender.SetPosition(i, transform.rotation * path[i] + transform.position);
            }
        }
    }

    public Quaternion GetCurrentRotation()
    {
        // Vector3 relativeCurrentPos = path[15] - path[0];
        Quaternion currentRotation = Quaternion.LookRotation(path[11], Vector3.up);
        //currentRotation = Quaternion.RotateTowards(Quaternion.LookRotation(Vector3.forward, Vector3.up), currentRotation, (15*rotationSmooth) * Time.deltaTime);
        return currentRotation;
    }


    public void PlaceObjectInPipe(GameObject toPalceObject, int pointOnPath, Vector2 pointInPipe)
    {
        ObservingObject newObserved = new ObservingObject(toPalceObject, pointOnPath, pointInPipe);
        observedGameobjects.Add(newObserved);
        newObserved.updatePos(path, gameObject.transform);
    }

    public void DisconnectObjectFromPipe(GameObject observedObject)
    {
        foreach(ObservingObject obsObj in observedGameobjects)
        {
            if(obsObj.gameObject == observedObject)
            {
                observedGameobjects.Remove(obsObj);
                return;
            }
        }
    }
}


class ObservingObject
{
    public GameObject gameObject;
    public int pathIndex;
    public Vector2 pointInPipe;

    public ObservingObject(GameObject gameObject, int pathIndex, Vector2 pointInPipe)
    {
        this.gameObject = gameObject;
        this.pathIndex = pathIndex;
        this.pointInPipe = pointInPipe;
    }

    public void updatePos(List<Vector3> path, Transform parent)
    {
        gameObject.transform.position = parent.rotation * path[pathIndex] + parent.position + new Vector3(pointInPipe.x, pointInPipe.y, 0);
    }

}