using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeMesh
{
    private Mesh mesh = new Mesh();
    private Vector3[] verticies;
    private int[] triangles;
    private int countVertices;
    private int pathLength;
    public int removedSegments = 0;

    public Mesh GeneratePipeOfPath(Vector3[] path, float radius, int countVertices, Vector3 offset)
    {
        this.countVertices = countVertices;
        pathLength = path.Length;
        verticies = new Vector3[(countVertices+1) * path.Length];
        triangles = new int[6 * (countVertices) * (path.Length - 1)];
        GenerateShape(path, radius, countVertices, offset);
        UpdateMesh();

        return mesh;
    }

    private void GenerateShape(Vector3[] path, float radius, int countVertices, Vector3 offset)
    {
        Vector3 pathDirection = Vector3.forward;

        

        for (int i = 0, z = 0; z < path.Length-1; z++)
        {
            pathDirection = (path[z+1] - path[z]).normalized;
            
            for (int j = 0; j <= countVertices; j++)
            {
                Vector3 vertex = path[z];
                Vector3 spread = Quaternion.AngleAxis((360.0f / (countVertices)) * j, pathDirection) * Vector3.right;
                // Vector3 left = Vector3.Cross(pathDirection, pathDirection).normalized;
                //Quaternion rotation = Quaternion.FromToRotation(spread, pathDirection);
                //rotation = Quaternion.Euler(pathDirection);
                //spread = rotation * spread;
                vertex += (spread.normalized) * radius;
                //vertex = rotation * vertex;
                //vertex += offset/2;
                //vertex = new Vector3(j, 0, z);
                //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                //cube.transform.position = vertex + offset;
                //cube.transform.localScale = Vector3.one * 0.1f;
                verticies[i] = vertex;
                i++;
            }
        }

        int vert = 0;
        int tris = 0;
        for (int z = 0; z < path.Length-1; z++)
        {
            for (int x = 0; x < countVertices; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + countVertices + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + countVertices + 1;
                triangles[tris + 5] = vert + countVertices + 2;
                vert++;
                tris += 6;
            }
        }
        /*
        triangles[tris + 0] = countVertices - 1;
        triangles[tris + 1] = countVertices * 2 - 1;
        triangles[tris + 2] = 0;
        triangles[tris + 3] = 0;
        triangles[tris + 4] = countVertices * 2 - 1;
        triangles[tris + 5] = countVertices + 1;
        vert++;
        tris += 6;
        */
    } 

    private void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = verticies;
        mesh.triangles = triangles;
        mesh.uv = GenerateUVs();
        mesh.RecalculateNormals();
    }

    public Vector2[] GenerateUVs()
    {
        var uvs = new Vector2[mesh.vertices.Length];
        //float offset = (verticies[countVertices + 1] - verticies[0]).magnitude;
        //Debug.Log(offset);
        int i = 0;
        for (float z = 0; z <= pathLength-1; z++)
        {
            for (float x = 0; x <= countVertices; x++)
            {
                uvs[i] = new Vector2(x/countVertices, (z+removedSegments)/10);
                i++;
            }
        }

        return uvs;
    }
    public Vector2[] GenerateUVs(float zOffset)
    {
        var uvs = new Vector2[mesh.vertices.Length];
        int i = 0;
        for (float z = 0; z <= pathLength - 1; z++)
        {
            for (float x = 0; x <= countVertices; x++)
            {
                uvs[i] = new Vector2(x / countVertices, (z / 10)-(zOffset));
                i++;
            }
        }

        return uvs;
    }
}
