using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipePath
{
    public int connectedParts = 15;
    Vector3 targetDirection;
    public PipePath()
    {

    }

    public Vector3[] generatePath(int length, float stepLength)
    {
        Vector3[] path = new Vector3[length];
        path[0] = Vector3.zero;
        targetDirection = Vector3.forward;
        Quaternion rotation = Quaternion.LookRotation(targetDirection, Vector3.up);
        Vector3 prevVector = Vector3.forward * stepLength;

        for (int i = 1; i < length; i++)
        {
            
            Vector3 nextPoint = path[i - 1] + rotation * prevVector;
            path[i] = nextPoint;
            prevVector = (path[i] - path[i - 1]).normalized * stepLength;
            //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //cube.transform.position = nextPoint;
            //cube.transform.localScale = Vector3.one * 1f;
            if (i % connectedParts == 0)
            {
                Vector3 newDirection = GetNewRandomTarget(targetDirection);
                targetDirection = newDirection;
                rotation = Quaternion.LookRotation(targetDirection, Vector3.up);
            }
        }

        return path;
    }

    private Vector3 GetNewRandomTarget(Vector3 currentDirection)
    {
        currentDirection = Vector3.forward;
        float radius = 0.05f;
        Vector2 circle = Random.insideUnitCircle * radius;
        Vector3 target = currentDirection + new Vector3(circle.x, circle.y - 0.001f, 0);
        return target.normalized;
    }

    public Vector3[] AppendPath(Vector3[] path, float stepLength)
    {
        Vector3[] newPath = new Vector3[connectedParts];
        //Vector3 targetDirection = path[path.Length-1] - path[path.Length-2];
        //targetDirection = targetDirection.normalized;
        //targetDirection = Vector3.forward;
        Vector3 newDirection = GetNewRandomTarget(targetDirection);
        targetDirection = newDirection;
        // newDirection = Vector3.forward;
        Vector3 previousPoint = path[path.Length - 1];
        //Debug.Log("New path");
        Quaternion rotation = Quaternion.LookRotation(newDirection, Vector3.up);
        Vector3 prevVector = (path[path.Length - 1] - path[path.Length - 2]).normalized * stepLength;

        for (int i = 0; i < newPath.Length; i++)
        {
            Vector3 nextPoint = previousPoint + rotation * prevVector;
            newPath[i] = nextPoint;
            prevVector = (newPath[i] - previousPoint).normalized * stepLength;
            previousPoint = nextPoint;
            //Debug.Log(nextPoint);
        }
        return newPath;
    }

}
