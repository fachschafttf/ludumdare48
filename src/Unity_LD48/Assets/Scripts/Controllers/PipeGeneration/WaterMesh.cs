using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Watermesh
{
    private Mesh mesh = new Mesh();
    private Vector3[] verticies;
    private int[] triangles;
    private int pathLength;

    public Mesh GeneratePipeOfPath(Vector3[] path, float radius, int countVertices, Vector3 offset)
    {
        pathLength = path.Length/2;
        verticies = new Vector3[2 * pathLength];
        triangles = new int[6 * (pathLength - 1)];
        GenerateShape(path, radius, countVertices, offset);
        UpdateMesh();

        return mesh;
    }

    private void GenerateShape(Vector3[] path, float radius, int countVertices, Vector3 offset)
    {
        Vector3 pathDirection = Vector3.forward;

        for (int i = 0, z = 0; z < pathLength - 1; z++)
        {
            pathDirection = (path[z + 1] - path[z]).normalized;
            Vector3 vertex = path[z];
            //Vector3 vertex = new Vector3(path[z].x, path[z].y, path[z].z);
            Vector3 spread = Quaternion.AngleAxis((360.0f / (countVertices)) * 9, pathDirection) * Vector3.right;
            vertex += (spread.normalized) * (radius * 1.5f);
            verticies[i] = vertex;
            /*
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            cube.transform.position = vertex;
            cube.transform.localScale = Vector3.one * 1f;
            */
            vertex = path[z];
            //vertex = new Vector3(path[z].x, path[z].y, path[z].z);
            spread = Quaternion.AngleAxis((360.0f / (countVertices)) * 15, pathDirection) * Vector3.right;
            vertex += (spread.normalized) * (radius * 1.5f);
            verticies[i+1] = vertex;
            /*
            cube = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            cube.transform.position = vertex;
            cube.transform.localScale = Vector3.one * 1f;
            */

            i += 2;
            
        }

      

        int vert = 0;
        int tris = 0;
        for (int z = 0; z < pathLength - 1; z++)
        {
         
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + 2;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + 2;
                triangles[tris + 5] = vert + 3;
                vert+=2;
                tris += 6;
            
        }

        for (int i = triangles.Length-4; i < triangles.Length; i++)
        {
            triangles[i] = pathLength * 2 - 4;
        }
        /*
        triangles[tris + 0] = countVertices - 1;
        triangles[tris + 1] = countVertices * 2 - 1;
        triangles[tris + 2] = 0;
        triangles[tris + 3] = 0;
        triangles[tris + 4] = countVertices * 2 - 1;
        triangles[tris + 5] = countVertices + 1;
        vert++;
        tris += 6;
        */
    } 

    private void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = verticies;
        mesh.triangles = triangles;
        mesh.uv = GenerateUVs();
        mesh.RecalculateNormals();
    }

    private Vector2[] GenerateUVs()
    {
        var uvs = new Vector2[mesh.vertices.Length];
        int i = 0;

        for (var z = 0; z < uvs.Length-3; z+=4)
        {
            //uvs[i] = new Vector2(x / 1, z / pathLength);
            uvs[z] = new Vector2(2, 0);
            uvs[z+1] = new Vector2(0, 0);
            uvs[z+2] = new Vector2(2, 0.5f);
            uvs[z+3] = new Vector2(0, 0.5f);

        }
        

        return uvs;
    }
}
