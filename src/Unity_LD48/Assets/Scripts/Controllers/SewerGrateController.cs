using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class SewerGrateController : MonoBehaviour
{
    private GameObject Target;
    public Vector3 startPosition;
    public PipeGenerator pipeGenerator;
    public int respawnRangeMin = 40;
    public int respawnRangeMax = 80;
    public float respawnCoolDown = 5f;
    bool inPipe = false;
    // Start is called before the first frame update
    void Start()
    {
        Target = GameObject.Find("GameField");
        startPosition = transform.position;
        pipeGenerator = GameObject.Find("PipeGenerator").GetComponent<PipeGenerator>();
        Invoke("addWallToPipe", Random.Range(0f, 5f));

    }

    public void LeftPipe() // LeftPipe is called when the Object leaves the pipe (after being placed there via pipeGenerator.PlaceObjectInPipe)
    {
        inPipe = false;
        gameObject.SetActive(false);
        Invoke("addWallToPipe", Random.Range(0f, respawnCoolDown));

    }
    void addWallToPipe()
    {
        pipeGenerator.PlaceObjectInPipe(this.gameObject, Random.Range(respawnRangeMin, Mathf.Max(respawnRangeMin+1,respawnRangeMax)), new Vector2(startPosition.x, startPosition.y));
        inPipe = true;
        gameObject.SetActive(true);


    }
}
