using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterController : MonoBehaviour
{

    [Header("Init Settings")]
    // movement of ripples on plane
    public float WaterSpeed;
    // Mutation speed of ripples on plane
    public float RippleSpeed;
    // Density of the ripples
    public float RippleDensity;
    // Start is called before the first frame update


    private Material shader;
    void Start()
    {
        InitializeShader();
    }

    // This initializes the shader with the start settings, so its easier to configure from inspector
    private void InitializeShader()
    {
        shader = GetComponent<MeshRenderer>().material;
        shader.SetFloat("RippleSpeed", RippleSpeed);
        shader.SetFloat("RippleDensity", RippleDensity);
        shader.SetFloat("WaterSpeed", WaterSpeed);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        UpdateWater();
    }

    private void UpdateWater()
    {
        shader.SetFloat("WaterSpeed", WaterSpeed);
        shader.SetFloat("RippleSpeed", RippleSpeed);
    }
}
