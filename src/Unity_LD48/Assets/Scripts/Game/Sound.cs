﻿using System;
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Sound
{
    [HideInInspector]
    public AudioSource source;
    public AudioClip clip;

    [Range(0, 1)]
    public float volume;

    public bool loop;

    public void Play(float masterVolume)
    {
        if (source != null)
        {
            source.volume = volume * masterVolume;
            source.Play();
        }
    }

    public void SetMasterVolume(float masterVolume)
    {
        source.volume = volume * masterVolume;
    }

    public void Stop()
    {
        source.Stop();
    }

}
