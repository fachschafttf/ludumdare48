using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public GameObject menuCanvas;
    public GameObject mobileControls;

    public float speed = 5f;
    public float maxVelocityChange = 10.0f;

    public float initialHealth = 100f;

    private Rigidbody _rigidbody;
    private Vector3 rawInputMovement = Vector3.zero;

    private float currentHealth;
    private bool killInLateUpdate = false;

    public float JumpForce = 100f;

    public GameObject highscore;

    private EventSystem _eventSystem;
    private GameObject model;
    private bool died = false;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        mobileControls = GameObject.Find("MobileControls");
        _eventSystem = mobileControls.GetComponentInChildren<EventSystem>();
        model = transform.Find("Model").gameObject;
        currentHealth = initialHealth;
    }

    // Update is called once per frame
    void Update()
    {
        // Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //Vector3 move = rawInputMovement * speed * Time.fixedDeltaTime;
        //_rigidbody.MovePosition(transform.position + move);

        // Add velocity change for movement on the local horizontal plane
        Vector3 targetVelocity = rawInputMovement * speed;
        Vector3 localVelocity = transform.InverseTransformDirection(_rigidbody.velocity);
        Vector3 velocityChange = transform.InverseTransformDirection(targetVelocity) - localVelocity;

        // The velocity change is clamped to the control velocity
        // The vertical component is either removed or set to result in the absolute jump velocity
        velocityChange = Vector3.ClampMagnitude(velocityChange, maxVelocityChange);
        velocityChange = transform.TransformDirection(velocityChange);
        _rigidbody.AddForce(velocityChange, ForceMode.Acceleration);
        model.transform.rotation = Quaternion.LookRotation(new Vector3((targetVelocity.x/200)/2, 0, 1), Vector3.up) * Quaternion.Euler(-90, 180, 0);

    }

    private void LateUpdate()
    {
        if (killInLateUpdate)
        {
            Die();
            //this.gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            highscore.GetComponent<HighScoreManager>().ResetMultiplier();
        }
        else if (collision.gameObject.tag == "Wall")
        {
            currentHealth = 0;
            killInLateUpdate = true;
        }
    }

    public void OnMovement(InputAction.CallbackContext value)
    {
        Vector2 inputMovement = value.ReadValue<Vector2>();
        rawInputMovement = new Vector3(inputMovement.x, 0, inputMovement.y);
    }

    public void OnJump(InputAction.CallbackContext value)
    {
        if(value.started)
        {
            Jump();
        }
    }

    public void addCollisionForce(Vector3 force)
    {
        Vector3 velocity = -_rigidbody.velocity;
        if (velocity.x * force.x <= 0f) velocity.x = 0f;
        if (velocity.y * force.y <= 0f) velocity.y = 0f;
        if (velocity.z * force.z <= 0f) velocity.z = 0f;
        _rigidbody.AddForce(velocity, ForceMode.VelocityChange);
        _rigidbody.AddForce(force, ForceMode.Acceleration);
    }

    public void damage(float amount)
    {
        //currentHealth -= amount;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            killInLateUpdate = true;
        }
        else
        {
            AudioController.Instance.PlaySound("Ouh" + Random.Range(0,3));
        }
        // Play damage sound

    }

    public void heal(float amount)
    {
        currentHealth += amount;
        // Play heal sound
    }

    private void Die()
    {
        if (died)
            return;
        died = true;
        Debug.Log("Game Over");
        transform.Find("KakePlatzen").gameObject.SetActive(true);
        transform.Find("Model").gameObject.SetActive(false);
        // Player dies
        AudioController.Instance.PlaySound("GameOver");
        StartCoroutine(Dieing(3));
    }

    IEnumerator Dieing(int secs)
    {
        yield return new WaitForSeconds(secs);
        DiedFinish();
    }

    public void DiedFinish()
    {
        this.gameObject.SetActive(false);
        menuCanvas.GetComponent<PauseMenu>().EndRun();
    }
    public void Jump()
    {
        var floater = GetComponent<FloatingController>();
        if (!floater.Flying)
        {
            AudioController.Instance.PlaySound("Wohoo" + Random.Range(0, 4));
            floater.Flying = true;
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            _rigidbody.AddForce(new Vector3(0, JumpForce, 0));
            transform.Translate(Vector3.up * 1f);
        }
    }
}
