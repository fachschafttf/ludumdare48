using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SocialPlatforms.Impl;

public class HighScoreManager : MonoBehaviour
{
    private float internalHighscore = 0f;
    public int highscore = 0;
    public float increase = 1f;
    public float multiplier = 1f;
    public float multiTimer = 5f;

    public GameObject noMulti;
    public GameObject yesMulti;
    public GameObject multiText;
    public GameObject mobileControls;
    public GameObject firstEndGameObject;
    public GameObject alertNameEnter;
    public TextMeshProUGUI countText;

    public Text HoFScore1;
    public Text HoFScore2;
    public Text HoFScore3;
    public Text HoFName1;
    public Text HoFName2;
    public Text HoFName3;
    public Text localHighcore;

    private float timeSince;


    private EventSystem _eventSystem;
    private string _playerUuid;
    
    // Firebase stuff
    [DllImport("__Internal")]
    public static extern void GetJSON(string path, string objectName, string callback, string fallback);
    
    [DllImport("__Internal")]
    public static extern void PostJSON(string path, string value, string objectName, string callback, string fallback);
    
    // Start is called before the first frame update
    void Start()
    {
        resetUUID();        
        highscore = 0;
        increase = 3f;
        SetCountText();
        
        mobileControls = GameObject.Find("MobileControls");
        _eventSystem = mobileControls.GetComponentInChildren<EventSystem>();

        noMulti.SetActive(true);
        yesMulti.SetActive(false);
        multiText.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        internalHighscore += increase * multiplier * Time.deltaTime;
        int tmp = (int) internalHighscore / 10;
        highscore = tmp * 10;
        SetCountText();

        //Increase Multiplyer after time
        timeSince += Time.deltaTime;
        if(timeSince > multiTimer)
        {
            IncreaseMultiplier();
            timeSince = 0f;
        }
    }

    public void GameEnd()
    {
        _eventSystem.SetSelectedGameObject(firstEndGameObject);
        int finalScore = highscore;
        if (finalScore > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", finalScore);
            localHighcore.text = finalScore.ToString();
        }
        localHighcore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();

        UpdateHallOfFame();
    }

    public void UpdateHallOfFame()
    {
        #if UNITY_EDITOR
        #else
        GetJSON("/", "HighScore", "SetHallOfFameUi", "fallback");
        #endif
    }
    public void SetHallOfFameUi(string hallOfFameString)
    {
        var array = hallOfFameString.Split('|');
        HoFName1.text = array[0].Split(':')[0];
        HoFScore1.text = array[0].Split(':')[1];
        HoFName2.text = array[1].Split(':')[0];
        HoFScore2.text = array[1].Split(':')[1];
        HoFName3.text = array[2].Split(':')[0];
        HoFScore3.text = array[2].Split(':')[1];
    }

    public void ResetLocalScore()
    {
        PlayerPrefs.DeleteKey("HighScore");
    }

    void SetCountText()
    {
        countText.text = highscore.ToString();
    }

    public void IncreaseMultiplier() {
        if(multiplier < 10f)
        {
            AudioController.Instance.PlaySound("PowerUp");
            if (multiplier == 1f)
            {
                multiplier += 1f;
            } else
            {
                multiplier += 2f;
            }
        }
        SetMultiSprite(multiplier);
    }

    public void ResetMultiplier()
    {
        if(multiplier != 1f)
        {
            AudioController.Instance.PlaySound("PowerDown");
        }
        multiplier = 1f;
        SetMultiSprite(multiplier);
    }

    public void BonusScore(float addScoreValue)
    {
        internalHighscore += addScoreValue;
    }

    public void SetMultiSprite(float _multiplier)
    {
        //activeSprite.SetActive(false);

        switch (_multiplier)
        {
            case 1:
                noMulti.SetActive(true);
                yesMulti.SetActive(false);
                multiText.SetActive(false);
                break;
            case 2:
                noMulti.SetActive(false);
                yesMulti.SetActive(true);
                multiText.GetComponent<Text>().text = "X2";
                multiText.SetActive(true);
                break;
            case 4:
                noMulti.SetActive(false);
                yesMulti.SetActive(true);
                multiText.GetComponent<Text>().text = "X4";
                multiText.SetActive(true);
                break;
            case 6:
                noMulti.SetActive(false);
                yesMulti.SetActive(true);
                multiText.GetComponent<Text>().text = "X6";
                multiText.SetActive(true);
                break;
            case 8:
                noMulti.SetActive(false);
                yesMulti.SetActive(true);
                multiText.GetComponent<Text>().text = "X8";
                multiText.SetActive(true);
                break;
            case 10:
                noMulti.SetActive(false);
                yesMulti.SetActive(true);
                multiText.GetComponent<Text>().text = "X10";
                multiText.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void SendHighScore(string playerId)
    {
        var obj = new Dictionary<string, dynamic>() {{"score", highscore}, {"name", $"\"{playerId}\""}};
        var jsonString = MyDictionaryToJson(obj);
        
        #if UNITY_EDITOR
            Debug.Log($"Post highscore: {jsonString}");
            alertNameEnter.SetActive(true);
            TextMeshProUGUI textmeshPro = alertNameEnter.GetComponentInChildren<TextMeshProUGUI>();
            textmeshPro.color = new Color32(255, 0, 0, 255);
            textmeshPro.text = "Upload not possible in unity";
        #else
                PostJSON(_playerUuid, jsonString, "HighScore", "JSuccess", "fallback");
                Debug.Log($"Post highscore: {jsonString}");
        #endif
        
    }

    private static string MyDictionaryToJson(Dictionary<string, dynamic> dict)
    {
        var entries = dict.Select(entry => $"\"{entry.Key}\": {entry.Value}").ToList();
        return "{" + string.Join(",", entries) + "}";
    }

    public void JSuccess(string log)
    {
        Debug.Log(log);
        alertNameEnter.SetActive(true);
        TextMeshProUGUI textmeshPro = alertNameEnter.GetComponentInChildren<TextMeshProUGUI>();
        textmeshPro.color = new Color32(100, 255, 0, 255);
        textmeshPro.text = "Success";
    }
    
    public void fallback(string log)
    {
        Debug.Log(log);
        alertNameEnter.SetActive(true);
        TextMeshProUGUI textmeshPro = alertNameEnter.GetComponentInChildren<TextMeshProUGUI>();
        textmeshPro.color = new Color32(255, 0, 0, 255);
        textmeshPro.text = "Error uploading, try again!";
    }

    public void resetUUID()
    {
        _playerUuid = System.Guid.NewGuid().ToString();
    }
    
    public void End()
    {
        increase = 0f;
    }
}
