using System.Runtime.InteropServices;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject SettingsPanel;
    public GameObject MainPanel;
    public GameObject mobileControls;
    public GameObject firstMainMenuElement;
    public GameObject firstSettingsElement;


    // Javascript stuff
    [DllImport("__Internal")]
    private static extern void GoFullscreen();
    [DllImport("__Internal")]
    private static extern void ExitFullscreen();
    
    private EventSystem _eventSystem;
    // Start is called before the first frame update
    void Start()
    {
        SettingsPanel.SetActive(false);
        MainPanel.SetActive(true);
        _eventSystem = mobileControls.GetComponentInChildren<EventSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        Debug.Log("Start Game Scene - Change Scene Name");
        AudioController.Instance.PlaySound("Beep" + Random.Range(0, 5));
        SceneManager.LoadScene("BaseLevel");
    }

    public void Multiplayer()
    {
        Debug.Log("Start Multiplayer Scene - Change Scene Name");
        AudioController.Instance.PlaySound("Beep" + Random.Range(0, 5));
        SceneManager.LoadScene("MainMenuDaniel");
    }

    public void QuitGame()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            ExitFullscreen();
            Debug.Log("close fullscreen");
        }
        else
        {
        #if UNITY_EDITOR
                    // Application.Quit() does not work in the editor so
                    // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
                    UnityEditor.EditorApplication.isPlaying = false;
        #else
                         Application.Quit();
        #endif
        }
    }

    public void Settings()
    {
        AudioController.Instance.PlaySound("Beep" + Random.Range(0, 5));
        MainPanel.SetActive(false);
        SettingsPanel.SetActive(true);
        SettingsPanel.GetComponent<SettingsSlider>().AdjustSlidersToCurrentLevel();
        _eventSystem.SetSelectedGameObject(firstSettingsElement);
    }

    public void SetVolume(float volume)
    {
        Debug.Log(volume);
        Debug.Log("TODO: Change Volume");
        AudioController.Instance.SetMusicVolume((int)volume);
    }

    public void SetSFXVolume(float volume)
    {
        AudioController.Instance.SetSfxVolume((int) volume);
        AudioController.Instance.PlaySound("Beep" + Random.Range(0, 5));
    }

    public void BackToMain()
    {
        AudioController.Instance.PlaySound("Beep" + Random.Range(0, 5));
        SettingsPanel.SetActive(false);
        MainPanel.SetActive(true);
        _eventSystem.SetSelectedGameObject(firstMainMenuElement);
    }
}
