using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawnController : MonoBehaviour
{
    public GameObject[] objects;                // The prefab to be spawned.
    public float spawnTime = 6f;            // How long between each spawn.
    public float xMin = -20;
    public float xMax = 20;
    public float zMin = 15;
    public float zMax = 60;
    private Vector3 spawnPosition;

    void Start()
    {
        // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
        InvokeRepeating("Spawn", spawnTime, spawnTime);

    }

    void Spawn()
    {
        spawnPosition.x = Random.Range(xMin, xMax);
        spawnPosition.y = -0.5f;
        spawnPosition.z = Random.Range(zMin, zMax);

        Instantiate(objects[Random.Range(0, objects.Length)], spawnPosition, Quaternion.identity);
    }
}
