using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;
#if UNITY_WEBGL && !UNITY_EDITOR
using System.Runtime.InteropServices;
#endif

public class PauseMenu : MonoBehaviour
{
    public static bool paused = false;
    public GameObject pauseMenuUI;
    public GameObject endScreenUI;
    public GameObject highscore;
    public GameObject mobileControls;
    public GameObject firstPauseMenuObject;
    public Text finalScoreUI;
    public Text localHighScoreUI;
    public GameObject alertNameEnter;
    
    
    public string playerID;

    private int score;
    private int localScore;
    private EventSystem _eventSystem;
    private HighScoreManager _highScoreManager;
    private UI.TouchInput _touchInput;

    // Javascript stuff
    [DllImport("__Internal")]
    private static extern void GoFullscreen();
    [DllImport("__Internal")]
    private static extern void ExitFullscreen();
    
    private void Start()
    {
        _highScoreManager = highscore.GetComponent<HighScoreManager>();
        pauseMenuUI.SetActive(false);
        endScreenUI.SetActive(false);
        mobileControls = GameObject.Find("MobileControls");
        _eventSystem = mobileControls.GetComponentInChildren<EventSystem>();
        _touchInput = mobileControls.GetComponentInChildren<UI.TouchInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTogglePause(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            if (paused)
            {
                Resume();
            } else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        _touchInput.Resume();
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        paused = false;
    }

    void Pause()
    {
        _eventSystem.SetSelectedGameObject(firstPauseMenuObject);
        _touchInput.Pause();
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        paused = true;
    }

    public void LoadMenu()
    {
        _highScoreManager.resetUUID();
        Debug.Log("Load Main Menu scene");
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu2");

    }

    public void EndRun()
    {
        Time.timeScale = 0f;
        score = _highScoreManager.highscore;
        _highScoreManager.GameEnd();
        localScore = PlayerPrefs.GetInt("HighScore");
        finalScoreUI.text = score.ToString();
        localHighScoreUI.text = localScore.ToString();
        pauseMenuUI.SetActive(false);
        endScreenUI.SetActive(true);
        _eventSystem.SetSelectedGameObject(_highScoreManager.firstEndGameObject);
        //Time.timeScale = 1f;
    }

    public void RestartScene()
    {
        Debug.Log("restart active Scene");
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        _highScoreManager.resetUUID();
    }

    public void SetPlayerID (string id)
    {
        playerID = id;
    }

    public void SubmitScore()
    {
        if (playerID == "")
        {
            alertNameEnter.SetActive(true);
            TextMeshProUGUI textmeshPro = alertNameEnter.GetComponentInChildren<TextMeshProUGUI>();
            textmeshPro.color = new Color32(255, 0, 0, 255);
            textmeshPro.text = "Enter a name first!";
        }
        else if (playerID.Contains("|") || playerID.Contains("°"))
        {
            alertNameEnter.SetActive(true);
            TextMeshProUGUI textmeshPro = alertNameEnter.GetComponentInChildren<TextMeshProUGUI>();
            textmeshPro.color = new Color32(255, 0, 0, 255);
            textmeshPro.text = "Enter a name without special characters!";
        }
        else {
            _highScoreManager.SendHighScore(playerID);
            _highScoreManager.UpdateHallOfFame();
        }
    }

    public void EndApplication()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            _highScoreManager.resetUUID();
            ExitFullscreen();
            Debug.Log("close fullscreen");
        }
        else
        {
        #if UNITY_EDITOR
                    // Application.Quit() does not work in the editor so
                    // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
                    UnityEditor.EditorApplication.isPlaying = false;
        #else
                 Application.Quit();
        #endif
        }
    }
}
