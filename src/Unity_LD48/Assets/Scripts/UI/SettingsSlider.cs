using UnityEngine;
using UnityEngine.UI;

public class SettingsSlider : MonoBehaviour
{

    public void AdjustSlidersToCurrentLevel()
    {
        var sliders = GameObject.FindGameObjectsWithTag("Slider");
        foreach (var slider in sliders)
        {
            if (slider.name == "Slider")
            {
                slider.GetComponent<Slider>().value = AudioController.Instance.MusicVolume;
            }
            else if (slider.name == "SliderSFX")
            {
                slider.GetComponent<Slider>().value = AudioController.Instance.SfxVolume;
            }
            else
            {
                Debug.LogError("Attention, slider with undefined behavior");
            }
        }
    }

}
