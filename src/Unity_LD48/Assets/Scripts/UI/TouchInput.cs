using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.Serialization;

namespace UI
{
    
    public class TouchInput : MonoBehaviour
    {
        public bool mobileControlsActive = false;
        public bool showMobileControlsIfActive = true;

        public GameObject joystickObject;

        private bool paused = false;
        

        // Start is called before the first frame update
        void Start()
        {
            mobileControlsActive = PlayerPrefs.GetInt("mobileControls", 0) > 0;
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        private void FixedUpdate()
        {
            if (!joystickObject && showMobileControlsIfActive) return;
            bool isActive = mobileControlsActive && !paused;
            if (joystickObject.activeInHierarchy != isActive)
            {
                joystickObject.SetActive(isActive);
            }
        }

        public void OnTouchInput(InputAction.CallbackContext context)
        {
            if (context.action.activeControl.device != Touchscreen.current) return;
            if (!mobileControlsActive)
            {
                mobileControlsActive = true;
                PlayerPrefs.SetInt("mobileControls", 1);
            }
        }

        public void Pause()
        {
            Debug.Log("PAUSE");
            joystickObject.SetActive(false);
            paused = true;
        }
        public void Resume()
        {
            Debug.Log("RESUME");
            paused = false;
        }
    }

}