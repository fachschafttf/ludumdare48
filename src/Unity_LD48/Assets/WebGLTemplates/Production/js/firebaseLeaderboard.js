﻿var leaderboard = []

var loadLeaderBoard = function() {
    firebase.database().ref().orderByChild("score").limitToLast(100).on('value', function (snapshot) {
        leaderboard = [];
        var unsorted = snapshot.val();
        for (var key in unsorted) {
            if (unsorted.hasOwnProperty(key)) {
                leaderboard.push({"score": unsorted[key]['score'].toString(), "name": unsorted[key]['name']});
            }
        }

        leaderboard.sort(function (a, b) {
            return b["score"] - a["score"];
        });
        var content = ""
        for (var key in leaderboard) {
            content += `<li class="list-group-item d-flex justify-content-between align-items-center">${leaderboard[key]["name"]}<span class="badge bg-primary">${leaderboard[key]["score"]}</span>
    </li>`
        }
        document.getElementById("leaderboard-content").innerHTML = content;
    });
}

loadLeaderBoard();