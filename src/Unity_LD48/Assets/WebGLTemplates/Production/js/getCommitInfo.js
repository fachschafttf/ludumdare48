﻿    var auto_reload = false;
    var commit = {}
    var get_commit = function() {
    let lastCommit = undefined;
    if (('COMMIT_SHA' in commit)) {
    lastCommit = commit['COMMIT_SHA'];
}
    let client = new XMLHttpRequest();
    client.open('GET', 'CI_COMMIT_INFO?' +Date.now());
    client.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
    commit = JSON.parse(client.responseText)
    if (lastCommit !== undefined) {
    if (commit['COMMIT_SHA'] !== lastCommit) {
    location.reload();
}
}
    else {
    document.getElementById("commit_sha").innerHTML = commit['COMMIT_SHA'];
    document.getElementById("commit_timestamp").innerHTML = commit['CI_COMMIT_TIMESTAMP'];
}
}
}
    client.send();
}
    if (auto_reload) {
    setInterval(function () {
        get_commit();
    }, 5000); // every 5s
}
    get_commit();