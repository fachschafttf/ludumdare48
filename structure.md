# Project structure

Please don't save your stuff everywhere. Instead use this structure. If you don't think there is already a good subfolder for something, create one!
```
src/                             # everything that is related to the project/gamejam
├── Music/                       # raw music files like soundfx, background music etc.
├── Models/                      # raw model files like blender etc.         
├── Sprites/                     # raw sprite files like png etc.
├── YOURRAWSTUFF/                # you have another raw format? create a new folder 
├── Unity_LD48/                  # The Unity3d Project, everything in here should be accessed at least once by unity3d before commiting
|   └── Assets/                  # Everything we create should be inside this
|       └── Scenes/              # no files inside this folder, use a subfolder
|           └── Levels           # the actual game, don't save temporary stuff here. should be empty for most of the weekend :wink:
|           └── Sandbox          # temporary game scenes. save everything you want to try here
|       └── Materials            # only materials
|       └── Models               # only models
|       └── Shader               # only shader stuff
|       └── Scripts              # no files inside this folder. use subfolder!
|           └── Game             # use subfolders for controllers, managers etc.
|           └── Player           # only player related
|           └── Enemies          # only Enemy related
|           └── Object           # only WorldObject related
|           └── Environment      # only Environment related
|           └── UI               # only UI related
|           └── Effects          # only Effects related
|           └── Sandbox          # eveything you just want to try out
|       └── Prefabs              # use subfolders to store your prefabs
|           └── Game             # use subfolders for controllers, managers etc.
|           └── Player           # only player related
|           └── Enemies          # only Enemy related
|           └── Object           # only WorldObject related
|           └── Environment      # only Environment related
|           └── UI               # only UI related
|           └── Sandbox          # eveything you just want to try out
|       └── Textures             # only textures
|   └── Library                  # Unity specific stuff, not in git
|   └── Logs                     # Unity specific, maybe useful to look in :wink:, not in git
|   └── ...                      # Unity specific stuff, not in git
|   └── Packages                 # Unity specific stuff
|   └── ProjectSettings          # Unity specific stuff
|   └── ...                      # Unity specific stuff, not in git
ci/                              # everything ci related
├── build.sh
└── ...
README.md
.gitignore
...
```
